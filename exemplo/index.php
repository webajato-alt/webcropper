<?php
include '../vendor/autoload.php';

use Crop\Crop;

if (isset($_FILES['imagem'])) {
    $c = new Crop();

    $c->setSizeNormal(1000);
    $c->setSizeThumb(300, 150);

    $c->upload(__DIR__ . '/arquivo.png', __DIR__ . '/arquivo_thumb.png');
}


?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <link rel="stylesheet" href="../src/assets/cropper.min.css">
    <link rel="stylesheet" href="../src/assets/webajato.cropper.css">
    <script src="../src/assets/jquery-3.6.0.min.js"></script>
    <script src="../src/assets/cropper.min.js"></script>
    <title>TESTE</title>
</head>
<body>
    <form action="" method="post" enctype="multipart/form-data">
        <div
            id="dv_image"
            data-src="../src/assets/upload.png"
            data-width="600"
            data-height="400"
            ></div>

        <button id="btn_salvar" type="submit" class="btn btn-success">Salvar</button>
    </form>
    <script src="../src/assets/webajato.cropper.js"></script>
    <script>
        webCropper.init(document.getElementById('dv_image'));
    </script>
</body>
</html>