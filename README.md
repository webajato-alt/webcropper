# WebCropper

Para instalar use:

```
composer require webajato/crop
```

# Veja exemplo na pasta exemplo

## Includa no Header do HTML

```html
    <link rel="stylesheet" href="assets/cropper.min.css">
    <link rel="stylesheet" href="assets/webajato.cropper.css">
    <script src="assets/cropper.min.js"></script>
```

Obs.: Obrigatório o uso de jquery.

## No html (no form)

```html
    <div
        id="dv_image"
        data-src="../src/assets/upload.png"
        data-width="600"
        data-height="400"
        ></div>
```


```html
    <script src="assets/webajato.cropper.js"></script>
    <script>
        webCropper.init(document.getElementById('dv_image'));
    </script>
```

## AVISO

Copiar os arquivos da `src/assets` para usa `public/assets/crop` ou raiz do site.

# Para salvar no PHP

Antes deverá ser definido os **PATH** e **URL** no arquivo `path.php` (dentro da pasta app). Exemplo:

```php

// ...
define('PATH', "/home/usuario/public_html/"); // Definido no arquivo app/config.php
define('URL', "/"); // Definido no arquivo app/config.php
// ...
define('PATH_PRODUTOS', \PATH . 'upload/produtos');
define('URL_PRODUTOS', \URL . 'upload/produtos');
// ...
```

```php
<?php
// include '../vendor/autoload.php';
namespace App\Controllers;

use Crop\Crop;

class ProdutoController {
    public function upload ()
    {
        if (isset($_FILES['imagem'])) {
            $c = new Crop();

            /*
            * Define dimensões:
            * 1. Parametro: Width
            * 2. Parametro: Height (opcional)
            */
            $c->setSizeNormal(800, 400); // Tamanho maximo da foto principal
            $c->setSizeThumb(300, 150); // Tamanho maximo da Thumb e proporção

            $c->upload(
                \PATH_PRODUTOS . '/arquivo.png', // Foto principal
                \PATH_PRODUTOS . '/arquivo_thumb.png' // Foto Thumb
            );
        }
    }
}

```

