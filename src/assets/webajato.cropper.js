
const webCropper = {
  updated: false,
  element: null,
  data: null,
  aspectRatio: 1,
  initialAspectRatio: 1,
  instanceCropper: null,
  timer: null,
  init: function (element) { //, image, aspectRatio = 1) {
    // const image = document.getElementById('image');
    this.element = element;
    this.render();

    /*
    let that = this;
    this.aspectRatio = aspectRatio;

    $('.webcropper .webcropper-btn').click((ev) => {
      ev.preventDefault();
      $('.webcropper input').click();
    });

    $('.webcropper input').change((ev) => {
      // console.log(ev);
      
      let files = ev.target.files;

        // FileReader support
        if (FileReader && files && files.length) {
            var fr = new FileReader();
            fr.onload = function () {
                // document.getElementById(outImage).src = fr.result;
                document.querySelector('.webcropper img').src = fr.result;
                that.refeshCrop();
            }

            fr.readAsDataURL(files[0]);
        }
    });

    this.instanceCropper = new Cropper(image, {
      aspectRatio: aspectRatio,
      crop(event) {
        that.data = event.detail;
        if (that.timer != null) {
          clearTimeout(that.timer);
          that.timer = null;
        }
        that.timer = setTimeout(() => { that.save() }, 500);
        that.onChange(event);
      }
    });
    */
  },

  render: function ()
  {
    let id = $(this.element).attr('id');
    let name = $(this.element).attr('data-name');
    let src = $(this.element).attr('data-src');
    let ratio = $(this.element).attr('data-ratio');
    let width = $(this.element).attr('data-width');
    let height = $(this.element).attr('data-height');
    let that = this;

    if (typeof id == 'undefined') {
      throw Error('Informe o Id da tag para usar o webcropper');
    }
    if (typeof name == 'undefined') name = 'imagem';
    if (typeof src == 'undefined') src = '';
    if (typeof width == 'undefined') width = 1;
    if (typeof height == 'undefined') height = width;
    if (typeof ratio == 'undefined') ratio = width / height;

    this.aspectRatio = ratio;

    let html = `<input type="file" name="${name}" accept="image/*">
            <img src="${src}">
            <div class="webcropper-btn">
                <button type="button" class="btn btn-success">Selecionar imagem</button>
            </div>`;

    $(this.element).addClass('webcropper');
    $(this.element).html(html);

    $('#'+id+'.webcropper .webcropper-btn').click((ev) => {
      ev.preventDefault();
      $('.webcropper input').click();
    });

    $('#'+id+'.webcropper input').change((ev) => {
      
      let files = ev.target.files;

        // FileReader support
        if (FileReader && files && files.length) {
            var fr = new FileReader();
            fr.onload = function () {
                // document.getElementById(outImage).src = fr.result;
                document.querySelector('.webcropper img').src = fr.result;
                that.refeshCrop();
            }

            fr.readAsDataURL(files[0]);
        }
    });

    // this.refeshCrop();
    $('.webcropper img').click(function (ev) {
      ev.preventDefault();
      $('.webcropper img').unbind();

      $('.webcropper input').click();
      // that.refeshCrop();
    });

  },
  refeshCrop: function ()
  {
    let that = this;
    let image = document.querySelector('.webcropper img');

    if (this.instanceCropper == null) {
      this.instanceCropper = new Cropper(image, {
          aspectRatio: this.aspectRatio,
          crop(event) {
            that.data = event.detail;
            that.updated = true;
            
            if (that.timer != null) {
              clearTimeout(that.timer);
              that.timer = null;
            }
            that.timer = setTimeout(() => { that.save() }, 500);
            that.onChange(event);
          }
        });
    }
    this.instanceCropper.replace(image.src).reset();
  },
  onChange: function (event)
  {
    // console.log('x', event.detail.x);
    // console.log('y', event.detail.y);
    // console.log('width', event.detail.width);
    // console.log('height', event.detail.height);
    // console.log('rotate', event.detail.rotate);
    // console.log('scaleX', event.detail.scaleX);
    // console.log('scaleY', event.detail.scaleY);
  },
  save: function ()
  {
    this.instanceCropper.getCroppedCanvas().toBlob((blob) => {
      console.log('salvo', blob);

      let file = new File([blob], "arquivo.png",{type: blob.type, lastModified: new Date().getTime() });

      let container = new DataTransfer();
      container.items.add(file);

      document.querySelector('.webcropper input').files = container.files;
    });
  }
};
