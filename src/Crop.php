<?php namespace Crop;

use WideImage\WideImage;

class Crop {
    private $sizeNormalWidth = 1000;
    private $sizeNormalHeight = null;
    private $sizeThumbWidth = 600;
    private $sizeThumbHeight = 400;
    public function setSizeNormal ($width, $height = null)
    {
        $this->sizeNormalWidth = $width;
        $this->sizeNormalHeight = $height;
    }
    public function setSizeThumb ($width, $height = null)
    {
        $this->sizeThumbWidth = $width;
        $this->sizeThumbHeight = $height;
    }
    public function upload ($pathDestino, $pathDestinoThumb = null)
    {
        if (isset($_FILES['imagem']) && is_uploaded_file($_FILES['imagem']['tmp_name'])) {

            move_uploaded_file($_FILES['imagem']['tmp_name'], $pathDestino);

            $image = WideImage::load($pathDestino);
            $resized = $image->resize($this->sizeNormalWidth, $this->sizeNormalWidth, 'inside', 'down');

            // if ($ext == 'jpg') {
            //     $resized->saveToFile($pathDestino, 80);
            // } elseif ($ext == 'png') {
                $resized->saveToFile($pathDestino, 6);
            // } else {
            //     $resized->saveToFile($pathDestino);
            // }

            if (!is_null($pathDestinoThumb)) {
                $resized = $image->resize($this->sizeThumbWidth, null, 'inside', 'down')
                    ->crop('center', 'center', '100%', intval($this->sizeThumbHeight * 100 / $this->sizeThumbWidth) . '%');

                // if ($ext == 'jpg') {
                //     $resized->saveToFile($pathDestinoThumb, 80);
                // } elseif ($ext == 'png') {
                    $resized->saveToFile($pathDestinoThumb, 6);
                // } else {
                //     $resized->saveToFile($pathDestinoThumb);
                // }
            }

            
        }

    }
}